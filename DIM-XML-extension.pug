doctype xml
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc2119 SYSTEM "http://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY dim-protocol-01 SYSTEM "references/reference.dim-protocol-01.xml">
// local reference to another doc
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>

// categories: std | bcp | info | exp | historic
rfc(category='std' number='2417676' consensus='false' ipr='trust200902' docName='dim-xml-extension-02' submissionType='en-smm-apc' xml:lang='en' version='3')
  front
    title(abbrev="DIM/XML extension") Distributed Information Management protocol XML extension
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    date(year='2020' month="March" day="17")

    keyword DIM

    abstract
      t
        | This document describes the DIM/XML extensions for the DIM protocol,
        | as used by the n_ToF experiment.

  middle
    section
      name Introduction
      t
        | The #[xref(target="dim-protocol-01") DIM protocol] is a communication system for
        | distributed/mixed environments, it provides a network transparent
        | inter-process communication layer.

      t
        | The DIM/XML extension describes the use of #[xref(target="XML") XML]
        | language as well as some schemas and service definitions to implement
        | higher level services using the #[xref(target="dim-protocol-01") DIM protocol].

      // This one is a 'must' :)
      section
        name Requirements Notation
        t
          | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
          | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
          | document are to be interpreted as described in BCP 14,
          | #[xref(target='RFC2119') RFC2119].

    section(anchor="Overview")
      name General Overview

    section
      name Services

      section
        name State Service

        t
          | This service is used to describe a state-machine, its root element
          | is a #[em state] which can contain:
        ul
          li #[strong value] tags which describes the possible states
          li #[strong error] tag which is deprecated
          li #[strong errors] tag which contains active #[em error] tags
          li #[strong warnings] tag which contains active #[em warning] tags
          li
            | #[strong value &amp; strValue] attributes which reflects the
            | attributes of the currently active state

        t The #[em value] tags have two attributes:
        ul
          li
            | #[strong value] an integer code, negative numbers being reserved
            | for built-in states.
          li #[strong strValue] a text that describes the state.

        t
          | The values #[tt -1] and #[tt -2] are reserved respectively by
          | #[tt "NOT READY"] and #[tt "ERROR"] states.

        t The #[em error] and #[em warning] tags contains the following information:
        ul
          li #[strong code] an integer attribute identifying the error.
          li a message content that give details about the error.

        t
          | The service #[bcp14 SHOULD] list all possible state values,
          |  Only one of those states can be active at a time, having its
          | #[em value] attribute set on the global #[em state] tag.

        t
          | Whenever an error is active the global #[em state] attribute
          | #[bcp14 MUST] be set to #[tt -2] and the #[em strValue] to
          | #[tt "ERROR"], an error is active when the errors list is not empty.

        t The top #[em error] is deprecated and #[bcp14 SHOULD NOT] be used.

        t
          | Both top levels #[em errors] and #[em warnings] tags #[bcp14 SHOULD]
          | always be present, even if empty.

        figure
          name State Service #[xref(target="RELAX_NG") relax-ng compact schema].
          sourcecode(type='rnc' src="DIM-XML-extension/state_service.rnc")

        figure
          name Example State Service
          sourcecode(type="xml" src="DIM-XML-extension/state_service_example.xml")


      section(anchor="Command_Service")
        name Command/Acknowledge Service

        t
          | The Command/Acknowledge service is a service pair that is used to
          | send acknowledged commands.

        t
          | The Command service #[bcp14 MUST] end with #[tt "/Cmd"] suffix and the
          | and the reply service with #[tt "/Ack"] suffix.

        t
          | The Command Service's #[em command] root element #[bcp14 MUST]
          | have a #[em key] integer attribute, this key is used to match
          | #[em acknowledge] notifications with requests.

        t
          | The Acknowledge Service's #[em acknowledge] root element
          | #[bcp14 MUST] have 3 attributes:
        ul
          li #[strong key] an integer matching the Command's key.
          li #[strong status] an integer defining the command status.
          li #[strong error] an integer defining the command error code.

        t
          | When a command is properly executed its #[em status] and #[em error]
          | acknowledge attributes #[bcp14 MUST] respectively be #[tt "2"] and
          | #[tt "0"], and a message #[bcp14 SHOULD] as the acknowledge text
          | content.

        t
          | Acknowledge events #[bcp14 SHOULD] be sent only to the DIM Client
          | that sent the request, similarly to DIM RPC services (see
          | #[xref(target="dim-protocol-01")]), current implementation sends
          | replies to all connected DIM Clients.

        t
          | When a command is rejected its #[em status] attribute
          | #[bcp14 SHOULD] be #[tt "3"], its #[em error] attribute and
          | text content #[bcp14 SHOULD] reflect the error.

        t
          | It is #[bcp14 RECOMMENDED] and considered a good practice to map
          | negative error numbers to POSIX errno values.

        figure
          name Command Service #[xref(target="RELAX_NG") relax-ng compact schema].
          sourcecode(type='rnc' src='DIM-XML-extension/command_service.rnc')

        figure
          name Acknowledge Service #[xref(target="RELAX_NG") relax-ng compact schema].
          sourcecode(type='rnc' src='DIM-XML-extension/command_ack_service.rnc')

      section
        name XML/RPC Service

        t
          | The XML/RPC Service is an RPC service as described in
          | #[xref(target="dim-protocol-01") DIM protocol].

        t
          | The command service #[bcp14 MUST] be named #[tt "/RpcIn"] and
          | the reply service #[tt "/RpcOut"], both having a #[tt "C"] signature
          | to transport XML documents.

        t
          | The payload content exchanged on those services #[bcp14 MUST] be
          | identical to the #[xref(target="Command_Service") Command Service], the
          | only difference being that the #[tt acknowledge] element sent along
          | with replies #[bcp14 MAY] contain additional content.

      section(anchor="DataSet_Service")
        name DataSet Service

        t
          | The DataSet service describes a set of data organized as an array,
          | its root element is a #[em dataset] which contains #[em data]
          | child elements.

        t The #[em data] elements have the following attributes:
        ul
          li #[strong name] the #[bcp14 OPTIONAL] data name or description.
          li #[strong index] a unique (for the DataSet) index identifying the data.
          li #[strong unit] an #[bcp14 OPTIONAL] text describing the value's unit.
          li #[strong type] the value type (see below).
          li #[strong value] the data value.

        t The data type can be one of:
        table
          name DataSet Types
          thead
            tr #[th Type] #[th Value]
          tbody
            tr #[td 1] #[td 32 bits signed integer]
            tr #[td 2] #[td 64 bits double floating point number]
            tr #[td 3] #[td String]
            tr #[td 4] #[td 64 bits signed integer]
            tr #[td 5] #[td Enumerated type]
            tr #[td 6] #[td 8 bits signed integer]
            tr #[td 7] #[td 16 bits signed integer]
            tr #[td 8] #[td 32 bits floating point number]
            tr #[td 9] #[td Boolean (0/1) value]
            tr #[td 11] #[td 32 bits unsigned integer]
            tr #[td 14] #[td 64 bits unsigned integer]
            tr #[td 16] #[td 8 bits unsigned integer]
            tr #[td 17] #[td 16 bits unsigned integer]

        figure
          name DataSet Service #[xref(target="RELAX_NG") relax-ng compact schema].
          sourcecode(type='rnc' src='DIM-XML-extension/dataset_service.rnc')

        section(anchor="Nested_DataSet")
          name Nested DataSet

          t
            | A #[em data] element with no #[em type] and #[em value] (or empty)
            | attributes #[bcp14 MAY] contain #[em data] child elements and
            | #[bcp14 MUST] be interpreted as a nested #[em dataset].

          t
            | #[strong Note:] #[em type] and #[em value] elements #[bcp14 MUST] be
            | either both set or not set, setting one and not the other is an error.

        section
          name Auto-index
          t
            | The #[strong index] attribute is #[em OPTIONAL], when index is not
            | set it #[bcp14 MUST] be evaluated as the increment of the last
            | #[em data] element index belonging to the same #[em dataset], or
            | #[em 0] if the #[em data] element is the first one in the dataset.

          t
            | #[strong Note:] some special attention #[bcp14 MUST] be given to
            | #[em dataset] mixing automatic indexes and regular indexes as
            | indexes #[bcp14 MUST] be unique in the document.

        section
          name Examples
          figure
            name Example DatasSet.
            sourcecode(type='XML' src='DIM-XML-extension/dataset_example.xml')

          figure
            name Example DataSet with child data.
            sourcecode(type='XML' src='DIM-XML-extension/dataset_child_example.xml')

      section(anchor="Parameters_Service")
        name Parameters Service

        t
          | The Parameters service combines a DataSet and an Acknowledge/Command
          | service implemented using the following suffixes:
        ul
          li #[strong /Cmd] the Command service.
          li #[strong /Ack] the Acknowledge service.
          li #[strong /Aqn] a service displaying the DataSet.

        t
          | The associated XML document root element is a #[em parameters]
          | element (instead of #[em dataset]),
          | its content is the same than #[xref(target="DataSet_Service") DataSet service].

        t
          | The first #[em value] element of the #[em parameters] root tag
          | #[bcp14 MUST] be a #[tt type="1"] 32 bits signed integer, its
          | index #[bcp14 MUST] be #[tt 0], its value #[bcp14 MUST] contain
          | the total number of parameters and its name #[bcp14 SHOULD] be
          | #[tt "Number of parameters"].

        figure
          name Example Parameters Acquisition Service
          sourcecode(type='rnc' src='DIM-XML-extension/parameters_aqn_example.xml')

        t
          | Some of the user implemented parameters #[bcp14 MAY] be hidden to
          | the #[em /Aqn] service, not being displayed, the service
          | #[bcp14 MAY] still handle requests on those hidden parameters.

        t
          | Hidden parameters are taken in account in the total number of
          | parameters, the number of parameters value #[bcp14 MAY] be greater
          | than the number of #[em value] tags.

        section
          name Updates

          t
            | To modify a Parameters service, #[em data] elements are sent in a
            | #[em parameters] element using the #[em /Cmd] command service,
            | those elements #[bcp14 MUST] have
            | #[em index], #[em type] and #[em value] attributes as described in
            | #[xref(target="DataSet_Service") DataSet service], #[em name]
            | and #[em unit] attributes being #[bcp14 OPTIONAL].

          t
            | Updating an #[em enum] typed data #[bcp14 SHOULD] only update its current value,
            | the desription part #[bcp14 SHOULD NOT] be sent along with the update request.

          t
            | Updates sent on #[xref(target="Nested_DataSet") Nested DataSets] #[bcp14 MUST]
            | only modify the #[em data] elements sent in the request, other parts of the
            | #[xref(target="Nested_DataSet") Nested DataSets] #[bcp14 SHOULD] be left untouched.
          t
            | Each #[em Nested DataSet] in a #[em Parameters service] can be
            | considered as a standalone #[em Parameters service].

          t
            | In general, #[em Parameters] modifications #[bcp14 SHOULD] impact only the parameters
            | sent in the request.

          t
            | A #[em data] element #[em type] #[bcp14 SHALL] never change,
            | depending on the implementation compatible types #[bcp14 MAY] be
            | accepted (ex: receiving an int8 in a bool),
            | converting the received value and preserving the original #[em data] type.

          t
            | It is #[bcp14 RECOMMENDED]
            | to send updates using the actual #[em data] element type.

          t Here's the schema for this #[em /Cmd] service:
          figure
            name Parameters Command Service #[xref(target="RELAX_NG") relax-ng compact schema].
            sourcecode(type='rnc' src='DIM-XML-extension/parameters_cmd.rnc')

          t An example payload:
          figure
            name Example Parameters Command.
            sourcecode(type='rnc' src='DIM-XML-extension/parameters_cmd_example.xml')

    section
      name Contributors
      // Optional section to mention contributors

  back

    references
      name References
      references
        name Normative References
        | &dim-protocol-01;
        | &rfc2119;

      references
        name Informative References

      references
        name URL References

        reference(anchor="XML" target="https://www.w3.org/TR/xml/")
          front
            title Extensible Markup Language (XML)
            author
              organization W3C

        reference(anchor="RELAX_NG" target="https://relaxng.org/")
          front
            title RELAX NG is a schema language for XML
            author(fullname="MURATA Makoto")
              address
                email EB2M-MRT@asahi-net.or.jp
            author(fullname="James Clark")
              address
                email jjc@jclark.com

    section
      name Change Log
      ul
        li #[strong 01] Initial version
        li #[strong 02] Adding nested DataSets and unsigned types
        li #[strong 03] Adding some details on nested DataSets and Parameters

    section
      name Open Issues
      ul
        li
          | Command Service is redundant with DIM RPC (it has been created
          | since DIM RPC implementation didn't support multithreaded
          | environments).
