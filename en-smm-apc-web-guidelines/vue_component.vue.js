//@flow

import AnimBlock from './BaseAnimationBlock';

export default {
  name: 'BaseModalLoader',
  components: { AnimBlock },
  props: { isLoading: { type: Boolean, default: false } }
};
