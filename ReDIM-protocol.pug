<?xml version='1.0' encoding='utf-8'?>
| <!DOCTYPE rfc SYSTEM "rfc2629-xhtml.ent" [
// external reference to RFCs
<!ENTITY rfc793 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.0793.xml">
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc3986 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.3986.xml">
<!ENTITY rfc5234 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5234.xml">
<!ENTITY rfc6455 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.6455.xml">
<!ENTITY rfc7230 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.7230.xml">
<!ENTITY rfc8259 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.8259.xml">
// local reference to another doc
<!ENTITY DIM SYSTEM "references/reference.dim-protocol-01.xml">
| ]>

// processing instructions
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>


// categories: std | bcp | info | exp | historic
rfc(category='std' number='2417700' consensus='false' ipr='trust200902' docName='redim-protocol-01' submissionType='en-smm-apc' xml:lang='en' version='3')
  front
    title(abbrev='ReDIM protocol') Representational State Transfer Distributed Information Management Protocol
    author(fullname='Sylvain Fargier' initials='S.F.' role='editor' surname='Fargier')
      organization CERN
      address
        postal
          street 1, Esplanade des Particules
          code 1217
          city Meyrin
          country Switzerland
        email sylvain.fargier@cern.ch
    date(year='2019' month="August" day="13")

    keyword DIM
    keyword ReDIM

    abstract
      t
        | This document describes the ReDIM protocol, a
        | HTTP and WebSocket
        | oriented re-work of the DIM protocol
        | developed by the EN-SMM-APC team.

  middle
    section
      name Introduction
      t
        | This document describes the ReDIM protocol, a
        | #[xref(target="RFC7230") HTTP] and #[xref(target="RFC6455") WebSocket]
        | oriented re-work of the #[xref(target="dim-protocol-01") DIM protocol]
        | developed by the EN-SMM-APC team.

      t
        | The #[xref(target="dim-protocol-01") DIM protocol] is a communication system for
        | distributed/mixed environments, it provides a network transparent
        | inter-process communication layer.

      t
        | This evolution of the protocol is meant to evolve existing
        | #[xref(target="RFC0793") TCP] and binary oriented protocol to web oriented
        | technologies, in order to directly integrate this protocol in
        | Web-based applications.

      // This one is a 'must' :)
      section
        name Requirements Notation
        t
          | The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
          | "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
          | document are to be interpreted as described in BCP 14,
          | #[xref(target='RFC2119' format='default') RFC2119].

    section(anchor="Overview")
      name General Overview

      t
        | The ReDIM protocol is an evolution of the DIM protocol, thus its core
        | features and capabilities have been kept and/or updated.

      t
        | Please refer to the #[xref(target="dim-protocol-01") DIM protocol]
        | documentation for the core features and constraints.

      t This figure shows how ReDIM components interact:
      figure
        name ReDIM General Overview
        artwork(align='center' type='ascii-art')
          .
            <![CDATA[
            +--------------+     Subscribe     +------------------------+
            |              +------------------->                        |
            | ReDIM Client |                   | ReDIM Service Provider |
            |              <-------------------+                        |
            +------+--^----+      Publish      +---------+--------------+
                   |  |                                  |
                   |  |                                  |
                   |  | DNS Reply       DNS Registration |
                   |  |                                  |
                   |  |      +-------------------+       |
                   |  +------+                   |       |
                   |         | ReDIM Name Server <-------+
                   +--------->                   |
                   DNS Query +-------------------+
            ]]>

      t Those components can be referred to as #[tt ReDIM Nodes].

      t The following endpoints #[bcp14 MUST] be implemented on #[tt ReDIM Nodes]:
      table
        thead
          tr #[th Endpoint] #[th Node] #[th Methods] #[th Documentation]
        tbody
          tr #[td /dis/subscribe] #[td ReDIM Service Provider] #[td GET, WebSocket] #[td #[xref(target="Subscribe")]]
          tr #[td /dis/command] #[td ReDIM Service Provider] #[td POST, WebSocket] #[td #[xref(target="Commands")]]
          tr #[td /dns/query] #[td ReDIM Name Server] #[td GET, WebSocket] #[td #[xref(target="DnsQuery")]]
          tr #[td /dns/register] #[td ReDIM Name Server] #[td PUT, WebSocket] #[td #[xref(target="DnsReg")]]

    section(anchor="Protocol")
      name Protocol

      section
        name Messages
        t
          | All messages #[bcp14 MUST] be encoded using
          | #[xref(target="RFC8259") JSON] format.

      section(anchor="Transport")
        name Transport

        t Two different types of transport #[bcp14 SHOULD] be implemented.

        t
          | #[xref(target="RFC6455") WebSocket] transport #[bcp14 SHOULD] be
          | implemented on all ReDIM Nodes endpoints.

        t
          | #[xref(target="RFC7230") HTTP/1.1] transport #[bcp14 MUST] be
          | implemented on all ReDIM Nodes endpoints.

        section(anchor="WebSocket")
          name WebSocket transport

          t
            | When using WebSocket transport, once the WebSocket is established,
            | each message exchanged between the server and client #[bcp14 MUST]
            | be a single #[xref(target="RFC8259") JSON] object.

        section(anchor="HTTP_1_1")
          name HTTP/1.1 transport

          t
            | Depending on the endpoint and request #[tt HTTP Streaming]
            | #[bcp14 MAY] be used to send updates.

          t
            | #[tt HTTP Streaming] is a push-style data transfer technique that
            | allows a web server to continuously send data to a client over
            | a single HTTP connection that remains open indefinitely
            | (see #[xref(target="HTTP_Streaming")] for further details).

          section(anchor="HTTP_query")
            name HTTP/1.1 query parameters

            t
              | Some part of the request message #[bcp14 MAY] be encoded in the
              | query part of HTTP request (mainly GET) #[xref(target="RFC3986") URI].

            t
              | The following query parameters boolean values #[bcp14 MUST] be
              | supported in query by ReDIM nodes:
            ul
              li #[strong 0, false, off] are falsey values.
              li #[strong 1, true, on are] are truthy values.

            t Any un-parsed value is assumed to be truthy.

      section(anchor="Subscribe")
        name Subscribe

        t
          | The publish/subscribe mechanism of ReDIM Service Provider Nodes
          | #[bcp14 MUST] be implemented on #[tt /dis/subscribe] endpoint.

        t
          | Example subscription request, subscribing to a service watching for
          | changes also retrieving the value every 10 seconds:
        figure
          name Example subscription request
          sourcecode(type="json" src="ReDIM-protocol/subscription_example.json")

        t The same request as an HTTP/1.1 request with query parameters:
        figure
          name Example HTTP subscription request
          sourcecode(type="text" src="ReDIM-protocol/subscription_example_http.json")

        t Subscriptions messages have the following properties:
        table(align='center')
          thead
            tr
              th Property
              th Type
              th Default Value
              th Description
          tbody
            tr #[td name] #[td string] #[td]
              td the service name to subscribe to
            tr #[td sid] #[td string|integer] #[td]
              td the service ID used to match requests with replies
            tr #[td watch] #[td boolean] #[td false]
              td watch the service for changes
            tr #[td timer] #[td integer] #[td 0]
              td update interval in seconds
            tr #[td initial] #[td boolean] #[td true]
              td whereas current value should be sent
            tr #[td stamped] #[td boolean] #[td false]
              td retrieve timestamp information with the value (and quality).
            tr #[td definition] #[td string] #[td]
              td optional service definition value (used by proxies).

        t
          | The #[tt sid] is #[bcp14 OPTIONAL] in requests, but it #[bcp14 SHOULD]
          | be used unless for single requests.

        section
          name HTTP mode
          t
            | HTTP mode #[bcp14 MUST] be implemented using a #[tt GET] method,
            | and it #[bcp14 MUST] support url-encoded query parameters as
            | described in #[xref(target="HTTP_query")].

          t
            | When #[tt timer] or #[tt watch] options are used the replies
            | #[bcp14 MUST] be sent using #[xref(target="HTTP_Streaming")],
            | leaving the connection open and sending updates to the client
            | until it decides to close the connection.

          t
            | To unsubscribe a service, the client should simply terminate the
            | connection.

        section
          name WebSocket mode
          t WebSocket mode #[bcp14 SHOULD] be supported on this endpoint.

          t
            | Several subscriptions can be made on a single WebSocket connection,
            | Subscriptions options and type can be updated by re-using the same
            | #[tt sid].

          t
            | To unsubscribe a service, a subscription request with
            | #[tt initial] set to false, timer and watch disabled #[bcp14 MUST]
            | be sent.

          t
            | The WebSocket is left opened by the server once a call has been
            | made, leaving the client the opportunity to make additional
            | subscriptions.

      section(anchor="Publish")
        name Publish
        t
          | Publish messages are sent by the ReDIM Service Provider Node as a reply
          | to subscription requests.

        t
          | Example publish message, sent in reply to the subscription request
          | example above:
        figure
          name Example publish reply
          sourcecode(type="json" src="ReDIM-protocol/publish_example.json")


        t Publish messages have the following properties:
        table(align='center')
          thead
            tr #[th Property] #[th Type] #[th Description]
          tbody
            tr #[td name] #[td string]
              td the service name being published
            tr #[td sid] #[td string|integer]
              td the service ID used to match the associated request (when present in the request)
            tr #[td value] #[td any]
              td service value
            tr #[td timestamp] #[td integer]
              td millisecond based unix timestamp (when requested)
            tr #[td quality] #[td integer]
              td service quality (when timestamp requested)

      section(anchor="Commands")
        name Commands

        t
          | The commands mechanism of ReDIM Service Provider Nodes
          | #[bcp14 MUST] be implemented on #[tt /dis/command] endpoint.

        t
          | Example command:
        figure
          name Example command
          sourcecode(type="json" src="ReDIM-protocol/command_example.txt")

        t Command messages have the following properties:
        table(align='center')
          thead
            tr
              th Property
              th Type
              th Default Value
              th Description
          tbody
            tr #[td name] #[td string] #[td]
              td the service name to send the command to
            tr #[td sid] #[td string|integer] #[td]
              td the service ID used to match requests with replies (when calling an RPC)
            tr #[td data] #[td any] #[td]
              td command payload argument
            tr #[td definition] #[td string] #[td]
              td optional definition value (used by proxies).

        t
          | The #[tt sid] property #[bcp14 MUST] be set only for when the
          | command is sent to an RPC service, the ReDIM Node #[bcp14 MUST]
          | then send a reply with the same #[tt sid].

        t
          | Example RPC call:
        figure
          name Example RPC call
          sourcecode(type="json" src="ReDIM-protocol/rpc_example.txt")

        section
          name HTTP mode
          t
            | HTTP mode #[bcp14 MUST] be implemented using a #[tt POST] method
            | on this endpoint, the message #[bcp14 MUST] be sent as
            | an #[tt application/json] compatible payload.

          t
            | When a regular command is called (#[tt sid] not set) the request
            | is terminated by the server once fired.

          t
            | When an RPC service is called (#[tt sid] set) the HTTP reply #[bcp14 MUST]
            | be terminated by the server once the reply has been sent (not
            | creating an HTTP Streaming connection).

          t
            | An empty content reply #[bcp14 MUST] be sent by the server when
            | with HTTP code #[tt 204 OK] on success.

        section
          name WebSocket mode
          t WebSocket mode #[bcp14 SHOULD] be supported on this endpoint.

          t
            | The WebSocket #[bcp14 MUST] be left opened by the server once a called has been
            | made, leaving the client the opportunity to make additional calls.

      section(anchor="DnsQuery")
        name DNS Queries
        t
          | The DNS query mechanism of ReDIM Name Server Nodes
          | #[bcp14 MUST] be implemented on #[tt /dns/query] endpoint.

        t
          | Example DNS query:
        figure
          name Example DNS query
          sourcecode(type="txt" src="ReDIM-protocol/dns_query_example.txt")

        t Query messages have the following properties:
        table(align='center')
          thead
            tr #[th Property] #[th Type] #[th Description]
          tbody
            tr #[td name] #[td string]
              td the service name being requested
            tr #[td sid] #[td string|integer]
              td the service ID used to match requests with replies (when calling an RPC)

        t
          | The #[tt sid] property #[bcp14 MUST] be set only for when the
          | command is sent to an RPC service, the ReDIM Node #[bcp14 MUST]
          | then send a reply with the same #[tt sid].

        section
          name HTTP mode

          t
            | Example HTTP DNS query:
          figure
            name Example HTTP DNS query
            sourcecode(type="txt" src="ReDIM-protocol/dns_query_http_example.txt")

          t
            | HTTP mode #[bcp14 MUST] be implemented using a #[tt GET] method,
            | and it #[bcp14 MUST] support url-encoded query parameters as
            | described in #[xref(target="HTTP_query")].

          t
            | The #[tt sid] property may be omitted, requesting a value only
            | once.

          t
            | When #[tt sid] property is set, the replies
            | #[bcp14 MUST] be sent using #[xref(target="HTTP_Streaming")],
            | leaving the connection open and sending updates to the client
            | until it decides to close the connection.

        section
          name WebSocket mode
          t WebSocket mode #[bcp14 SHOULD] be supported on this endpoint.

          t
            | The WebSocket #[bcp14 MUST] be left opened by the server once a called has been
            | made, leaving the client the opportunity to make additional queries.

      section(anchor="DnsReply")
        name DNS Replies
        t
          | DNS Reply messages are sent by the ReDIM Name Server as a reply
          | to DNS Query requests.

        t
          | Example DNS reply message, sent in reply to the query
          | example above:
        figure
          name Example DNS reply
          sourcecode(type="txt" src="ReDIM-protocol/dns_reply_example.txt")

        t DNS Reply messages have the following properties:
        table(align='center')
          thead
            tr #[th Property] #[th Type] #[th Description]
          tbody
            tr #[td definition] #[td string]
              td the service definition
            tr #[td sid] #[td string|integer]
              td the service ID used to match the associated query (when present in the query)
            tr #[td node] #[td string]
              td service hosting node name
            tr #[td task] #[td string]
              td service hosting task
            tr #[td address] #[td integer]
              td deprecated node address (use node property)
            tr #[td pid] #[td integer]
              td the node pid
            tr #[td port] #[td integer]
              td the node service port
            tr #[td protocol] #[td integer]
              td node protocol version
            tr #[td format] #[td integer]
              td deprecated node binary encoding format

      section(anchor="DnsReg")
        name DNS Registration
        t
          | The DNS registration mechanism of ReDIM Name Server Nodes
          | #[bcp14 MUST] be implemented on #[tt /dns/register] endpoint.

        t Registration messages have the following properties:
        table(align='center')
          thead
            tr #[th Property] #[th Type] #[th Description]
          tbody
            tr #[td node] #[td string]
              td service hosting node name
            tr #[td task] #[td string]
              td service hosting task
            tr #[td address] #[td integer]
              td deprecated node address (use node property)
            tr #[td pid] #[td integer]
              td the node pid
            tr #[td port] #[td integer]
              td the node service port
            tr #[td protocol] #[td integer]
              td node protocol version
            tr #[td format] #[td integer]
              td deprecated node binary encoding format
            tr #[td services] #[td= 'array<service>']
              td list of services to register

        t
          | The services property is an array of objects that have the following
          | properties:
        table(align='center')
          thead
            tr #[th Property] #[th Type] #[th Description]
          tbody
            tr #[td sid] #[td string|integer]
              td the service ID used to identify this service
            tr #[td name] #[td string]
              td service name
            tr #[td definition] #[td string]
              td the complete service definition, including "CMD" or "RPC" suffix
            tr #[td remove] #[td boolean]
              td an optional flag to inform that the service has been removed

        t
          | The #[tt remove] property #[bcp14 SHOULD] only be set to remove
          | services.

        section
          name HTTP mode
          t
            | HTTP mode #[bcp14 MUST] be implemented using a #[tt PUT] method.

          t
            | The server #[bcp14 MUST] use #[xref(target="HTTP_Streaming")],
            | leaving the connection open for the ReDIM Name Server to send
            | #[xref(target="DnsCommand") DNS Commands] to the ReDIM
            | Service Provider.

          t
            | The ReDIM Service Provider is un-registered whenever this
            | connection is closed.

        section
          name WebSocket mode
          t WebSocket mode #[bcp14 SHOULD] be supported on this endpoint.

      section(anchor="DnsCommand")
        name DNS Commands

        t
          | DNS Commands are sent by the ReDIM Name Server to a connected ReDIM
          | Service Provider, those commands are objects that #[bcp14 MUST] have
          | a #[tt command] string property, containing one of:
        table(align='center')
          thead
            tr #[th Command] #[th Description]
          tbody
            tr #[td #[xref(target="DnsCommandRegister") register]] #[td registration request]
            tr #[td #[xref(target="DnsCommandDuplicate") duplicate]] #[td duplicate service or task notification]
            tr #[td #[xref(target="DnsCommandExit") stop, exit, soft_exit]] #[td service shutdown request]

        section(anchor="DnsCommandRegister")
          name DNS Register Command

          t DNS Register Command example:
          figure
            name Example DNS Register Command
            sourcecode(type="json" src="ReDIM-protocol/register_command.json")

          t
            | The DNS Register Command is used to request a ReDIM Service
            | Provider to send a complete registration message.

          t
            | Once this packet has been sent,
            | if no #[xref(target="DnsReg") DNS Registration message] is received
            | for an additional 108 seconds, then the ReDIM Service Provider
            | #[bcp14 SHOULD] terminate the connection.

        section(anchor="DnsCommandDuplicate")
          name DNS Duplicate Command
          t
            | The #[tt DNS Duplicate Command] is used to warn
            | a ReDIM Service Provider that some of its services, or its Task Name
            | are already registered by another ReDIM Service Provider.

          t
            | A Task Name collision is detected when one or several built-in
            | services are already registered, the #[tt DNS Duplicate Command]
            | message #[bcp14 MUST] have a #[tt task] string property set in this
            | specific case, matching the following #[xref(target="RFC5234") abnf syntax]:
          figure
            name Duplicate Task property syntax
            sourcecode(type='abnf')
              | dup_task = task_name "(" node_pid ")" "@" node_name ":" node_port
              | ; task_name and node_name are described in DIM documentation
              | node_pid = 1*DIGIT
              | node_port = 1*DIGIT

          t DNS Duplicate Task Name Command example:
          figure
            name Example Duplicate Task Name Command
            sourcecode(type="json" src="ReDIM-protocol/duplicate_task_name_command.json")

          t
            | To notify a Service Name collision the #[tt DNS Duplicate Command]
            | message #[bcp14 MUST] have a #[tt services] object property, that
            | maps task info (same syntax than Duplicate Task) to an array of
            | #[tt sid] identifying the colliding services.

          t DNS Duplicate Service Name Command example:
          figure
            name Example DNS Duplicate Service Name Command
            sourcecode(type="json" src="ReDIM-protocol/duplicate_service_name_command.json")

          t
            | The ReDIM Service Provider #[bcp14 SHOULD] stop its operation
            | whenever a Duplicate Task Name notification is received, but
            | it #[bcp14 SHOULD] continue to operate on Duplicate
            | Service notification, services notified as duplicate #[bcp14 MUST NOT] be
            | registered by the ReDIM Name Server.

        section(anchor="DnsCommandExit")
          name DNS Stop, Exit, Soft Exit Commands

          t
            | The ReDIM Service Provider #[bcp14 SHOULD] stop its operation
            | whenever one of those commands is received, it #[bcp14 SHOULD]
            | not try to reconnect once such a command has been received.

          t
            | The ReDIM Name Server MUST terminate the connection once this
            | command has been sent.

          t
            | The #[tt Soft Exit] command has an extra #[tt info] integer
            | attribute that is sent along with the command, this attribute
            | can be used as an error code number, its definition is left
            | unspecified and depends on the implementation.

          t DNS Soft Exit example:
          figure
            name Example DNS Soft Exit Command
            sourcecode(type="json" src="ReDIM-protocol/soft_exit_command.json")

      section(anchor="KeepAlive")
        name Keep Alive

        t Every transport #[bcp14 MAY] handle Keep Alive packets.
        t A Keep Alive packet has a special JSON attribute:
        figure
          name KeepAlive JSON Object
          sourcecode(type="json" src="ReDIM-protocol/keepAlive.json")
        t
          | Every object containing data in addition to the #[em keepAlive] attribute
          | #[bcp14 MUST] be handled as KeepAlive packet and consequently ignored.

    section(anchor="Proxy")
      name Proxying ReDIM protocol

      t
        | A ReDIM to DIM proxy implementation is available on
        | #[xref(target="ReDIM_DIM")].

      t
        | This implementation provides the following endpoints:
      ul
        li #[tt #[strong= '/server[:port]/dns/query']] to access a dns
        li #[tt #[strong= '/server:port/dis/subscribe']] to subscribe services
        li #[tt #[strong= '/server:port/dis/command']] to send commands

    section(anchor="Client")
      name ReDIM clients

      t
        | A Node.js and browsers compatible client implementation is available on
        | #[xref(target="ReDIM_CLIENT")].

    section
      name Contributors
      ul
        li= 'Mathieu Donze <mathieu.donze@cern.ch>'
        li= 'Maria Fava <maria.fava@cern.ch>'

  back

    references
      name References
      references
        name Normative References
        | &rfc793;
        | &rfc2119;
        | &rfc3986;
        | &rfc5234;
        | &rfc6455;
        | &rfc7230;
        | &rfc8259;

        | &DIM;

      references
        name URL References
        reference(anchor='HTTP_Streaming' target='https://www.pubnub.com/learn/glossary/what-is-http-streaming/')
          front
            title HTTP Streaming definition
            author
              organization PubNub
            date(year='2019')

        reference(anchor='ReDIM_DIM' target='https://gitlab.cern.ch/apc/experiments/ntof/redim-dim.js')
          front
            title ReDIM to DIM Proxy
            author
              organization CERN
            date(year='2019')

        reference(anchor='ReDIM_CLIENT' target='https://gitlab.cern.ch/apc/experiments/ntof/redim-client.js')
          front
            title ReDIM Node.js/web client
            author
              organization CERN
            date(year='2019')

    section
      name Change Log
      t A list of changes

    section
      name Open Issues
      t A list of open issues regarding this document
