# EN-SMM-APC documentation stream

Here are source documents for the EN-SMM-APC stream.

Documents are published on [APC docs](http://apc-dev.web.cern.ch/docs/).

## Documents generation

To generate documentation it is recommended to use the docker container as described in [xml2rfc README](https://gitlab.cern.ch/apc/common/xml2rfc/blob/master/README.md):
